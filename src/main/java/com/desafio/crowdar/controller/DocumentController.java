package com.desafio.crowdar.controller;

import java.util.Map;

// import java.io.IOException;

import com.desafio.crowdar.models.dao.IDocumentDao;
import com.desafio.crowdar.models.dao.IUserDao;
// import com.desafio.crowdar.storage.StorageService;
import com.desafio.crowdar.models.entity.Document;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.RequestParam;
// import org.springframework.web.bind.annotation.ResponseBody;
// import org.springframework.web.multipart.MultipartFile;
// import java.util.stream.Collectors;

// import org.springframework.core.io.Resource;
// import org.springframework.http.HttpHeaders;
// import org.springframework.http.ResponseEntity;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
// import org.springframework.web.servlet.mvc.support.RedirectAttributes;

// import com.desafio.crowdar.storage.StorageFileNotFoundException;

@Controller
public class DocumentController {

    @Autowired
    private IDocumentDao documentoDao;
	@Autowired
    private IUserDao userDao;

    @RequestMapping(value = "/listar-documentos", method = RequestMethod.GET)
    public String listar(Model model) {
        model.addAttribute("titulo", "Documentos");
        model.addAttribute("documentos", documentoDao.findAll());
        return "lista-documentos";
    }
    
	@RequestMapping(value = "/subir-documento", method = RequestMethod.GET)
    public String crear(Map<String, Object> model) {
        Document document = new Document();
        model.put("documento", document);
        model.put("usuarios", userDao.findAll());
        model.put("titulo", "Nuevo documento");
        return "form-documento";
    }

	@RequestMapping(value = "/subir-documento", method = RequestMethod.POST)
    public String guardar(Document document) {
        documentoDao.save(document);
        return "redirect:/listar-documentos";
    }

    // private StorageService storageService;

	// @Autowired
	// public DocumentController(StorageService storageService) {
	// 	this.storageService = storageService;
	// }

	// @GetMapping("/subir-documento")
	// public String listUploadedFiles(Model model) throws IOException {

	// 	model.addAttribute("files", storageService.loadAll().map(
	// 			path -> MvcUriComponentsBuilder.fromMethodName(DocumentController.class,
	// 					"serveFile", path.getFileName().toString()).build().toUri().toString())
	// 			.collect(Collectors.toList()));

	// 	return "form-documento";
	// }

	// @GetMapping("/files/{filename:.+}")
	// @ResponseBody
	// public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

	// 	Resource file = storageService.loadAsResource(filename);
	// 	return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
	// 			"attachment; filename=\"" + file.getFilename() + "\"").body(file);
	// }

	// @PostMapping("/")
	// public String handleFileUpload(@RequestParam("file") MultipartFile file,
	// 		RedirectAttributes redirectAttributes) {

	// 	storageService.store(file);
	// 	redirectAttributes.addFlashAttribute("message",
	// 			"You successfully uploaded " + file.getOriginalFilename() + "!");

	// 	return "redirect:/";
	// }

	// @ExceptionHandler(StorageFileNotFoundException.class)
	// public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
	// 	return ResponseEntity.notFound().build();
	// }

}