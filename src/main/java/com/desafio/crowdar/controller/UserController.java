package com.desafio.crowdar.controller;

import java.util.Map;

import com.desafio.crowdar.models.dao.IUserDao;
import com.desafio.crowdar.models.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

    @Autowired
    private IUserDao userDao;

    @RequestMapping(value = "/listar-usuarios", method = RequestMethod.GET)
    public String listar(Model model) {
        model.addAttribute("titulo", "Usuarios");
        model.addAttribute("usuarios", userDao.findAll());
        return "lista-usuarios";
    }

    @RequestMapping(value = "/crear-usuario", method = RequestMethod.GET)
    public String crear(Map<String, Object> model) {
        User user = new User();
        model.put("usuario", user);
        model.put("titulo", "Nuevo usuario");
        return "form-usuario";
    }

    @RequestMapping(value = "/crear-usuario/{id}", method = RequestMethod.GET)
    public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {
        User user = null;
        if (id > 0) {
            user = userDao.findById(id);
        } else {
            return "redirect:/listar-usuarios";
        }
        model.put("usuario", user);
        model.put("titulo", "Editar usuario");
        return "form-usuario";
    }

    @RequestMapping(value = "/crear-usuario", method = RequestMethod.POST)
    public String guardar(User user) {
        userDao.save(user);
        return "redirect:/listar-usuarios";
    }

    @RequestMapping(value = "/eliminar-usuario/{id}")
    public String delete(@PathVariable(value = "id") Long id) {
        if (id > 0) {
            userDao.delete(id);
        }
        return "redirect:/listar-usuarios";
    }

}
