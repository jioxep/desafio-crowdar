package com.desafio.crowdar;

// import com.desafio.crowdar.storage.StorageService;

// import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// import org.springframework.boot.context.properties.EnableConfigurationProperties;
// import org.springframework.context.annotation.Bean;
// import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@SpringBootApplication
// @EnableConfigurationProperties(StorageProperties.class)
public class CrowdarApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrowdarApplication.class, args);
	}

	// @Bean
	// CommandLineRunner init(StorageService storageService) {
	// 	return (args) -> {
	// 		storageService.deleteAll();
	// 		storageService.init();
	// 	};
	// }

}
