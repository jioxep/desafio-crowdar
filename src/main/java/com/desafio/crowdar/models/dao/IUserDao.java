package com.desafio.crowdar.models.dao;
import java.util.List;

import com.desafio.crowdar.models.entity.User;

public interface IUserDao{
    
    public List<User> findAll();
    
    public void save(User user);

    public User findById(Long id);

    public void delete(Long id);
}