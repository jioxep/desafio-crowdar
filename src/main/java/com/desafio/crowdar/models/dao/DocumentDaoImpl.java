package com.desafio.crowdar.models.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.desafio.crowdar.models.entity.Document;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class DocumentDaoImpl implements IDocumentDao{

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    @Override
    public List<Document> findAll() {
        return entityManager.createQuery("from Document").getResultList();
    }

    @Override
    @Transactional
    public void save(Document document) {
        if (document.getId() != null && document.getId() > 0) {
            entityManager.merge(document);
        } else {
            entityManager.persist(document);
        }
    }
    
}
