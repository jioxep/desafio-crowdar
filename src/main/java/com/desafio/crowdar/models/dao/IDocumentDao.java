package com.desafio.crowdar.models.dao;
import java.util.List;

import com.desafio.crowdar.models.entity.Document;

public interface IDocumentDao{
    public List<Document> findAll();

    public void save(Document document);
}